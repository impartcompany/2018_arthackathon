.. ArtHackathon documentation master file, created by
   sphinx-quickstart on Wed May  2 12:37:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

2018 <예술 해커톤 : 차세대 문화예술교육>
===========================================

**한국문화예술교육진흥원과 (재)예술경영지원센터는 문화예술교육, 4차 산업혁명 핵심기술 (인공지능, 빅데이터, 가상현실 등) 전문가 및 전공자들이 함께 협업하여 차세대 융합 문화예술교육 모델 및 예술교육 콘텐츠 아이디어를 발굴하는 <예술 해커톤: 차세대 문화예술교육>을 개최합니다.**


.. toctree::
   :maxdepth: 2

   intro
   detail
   apply
   attention
   faq
   table

